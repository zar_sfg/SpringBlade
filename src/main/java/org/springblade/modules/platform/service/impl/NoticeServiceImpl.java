package org.springblade.modules.platform.service.impl;

import org.springframework.stereotype.Service;

import org.springblade.core.base.service.BaseService;
import org.springblade.modules.platform.model.Notice;
import org.springblade.modules.platform.service.NoticeService;

@Service
public class NoticeServiceImpl extends BaseService<Notice> implements NoticeService {

}
