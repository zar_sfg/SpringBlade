package org.springblade.modules.platform.service;

import org.springblade.core.base.service.IService;
import org.springblade.modules.platform.model.Notice;

public interface NoticeService extends IService<Notice> {

}
